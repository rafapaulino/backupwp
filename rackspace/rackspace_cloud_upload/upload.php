<?php
// include the API
require('cloudfiles/cloudfiles.php');

// cloud info
$username = ""; // username
$key = ""; // api key

// Connect to Rackspace
$auth = new CF_Authentication($username, $key);
$auth->authenticate();
$conn = new CF_Connection($auth);

// Get the container we want to use
$container = $conn->get_container('ContainerName');

// store file information
$localfile = $_FILES['upload']['tmp_name'];
$filename = $_FILES['upload']['name'];

// upload file to Rackspace
$object = $container->create_object($filename);
$object->load_from_filename($localfile);
?>
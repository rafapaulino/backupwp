<?php
Class Conexao 
{

	private $_con;

	public function __construct() 
	{
		$this->_con = new PDO("mysql:host=localhost;dbname=findyourself12", "root", ""); 
	}

	public function cadastrar_token($token)
	{
		$userInfo = array(
			'user_id' => 1,
			'token' => $token,
			'created_at' => date("Y-m-d H:i:s")
		);
		$serialize = serialize($userInfo);
		$option_name = 'wp_dropbox_token';
		$stmt = $this->_con->prepare("INSERT INTO wp_options(option_name, option_value) VALUES(:option_name, :option_value)");
		$stmt->bindParam(':option_name',$option_name,PDO::PARAM_STR);
		$stmt->bindParam(':option_value',$serialize,PDO::PARAM_STR);
		$stmt->execute();
	}
}



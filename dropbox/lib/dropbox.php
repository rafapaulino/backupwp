<?php
session_start();
session_regenerate_id();
//https://daveismyname.com/backup-to-dropbox-with-php-bp
//https://www.dropbox.com/developers-v1/core/start/php
require 'vendor/autoload.php';
//use \Dropbox as dbx;
$appInfo = \Dropbox\AppInfo::loadFromJsonFile("dropbox.json");
$webAuth = new \Dropbox\WebAuthNoRedirect($appInfo, "PHP-Example/1.0");
$authorizeUrl = $webAuth->start();

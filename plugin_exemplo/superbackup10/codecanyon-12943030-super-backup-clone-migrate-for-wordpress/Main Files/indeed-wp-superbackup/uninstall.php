<?php 
if ( !defined( 'WP_UNINSTALL_PLUGIN' ) ) exit();
global $wpdb;
$tables = array('indeed_backups', 'indeed_backup_metas', 'indeed_destinations', 'indeed_destination_metas', 'indeed_logs');
foreach ($tables as $table){
	$wpdb->query("DROP TABLE `" . $wpdb->prefix . $table . "`;");
}

//delete temporary dir
$temp_dir = get_option('ibk_backup_dir');
$tempDir = ($temp_dir) ? WP_CONTENT_DIR . '/uploads/' . $temp_dir : WP_CONTENT_DIR . '/uploads/isnapshots';

$dirs = array($tempDir, WP_CONTENT_DIR . '/uploads/indeed-backups');
foreach ($dirs as $dir){
	indeed_delete_dir_recursive($dir);
}
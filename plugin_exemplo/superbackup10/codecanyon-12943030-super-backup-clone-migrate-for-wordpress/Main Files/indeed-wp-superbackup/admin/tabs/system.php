<?php
global $wp_version;
global $wpdb;
$themes_dir = WP_CONTENT_DIR . '/themes/';
$plugin_dir = WP_CONTENT_DIR . '/plugins/';
$uploads_dir = WP_CONTENT_DIR . '/uploads/';
$php_ver = phpversion();
$mysql_ver = $wpdb->db_version();

$wp_cron = ( defined('DISABLE_WP_CRON') && DISABLE_WP_CRON ) ? 'Disabled' : 'Enabled';

$safe_mode = (ini_get('safe_mode')) ? 'On' : 'Off';

$php_zip = (class_exists('ZipArchive')) ? 'On' : 'Off';

$memory_limit = ini_get('memory_limit');

$max_execution_time = ini_get('max_execution_time');

$open_ssl = (extension_loaded('openssl')) ? 'On' : 'Off';

$curl = (function_exists('curl_version')) ? 'On' : 'Off';

$wp_alternative_cron = ( defined('ALTERNATE_WP_CRON') && ALTERNATE_WP_CRON ) ? 'Enabled' : 'Disabled';

$spl_file_object = (class_exists('SplFileObject')) ? 'On' : 'Off';

$recursive_iterator = (class_exists('RecursiveIteratorIterator')) ? 'On' : 'Off';
?>

<div class="ibk-settings-wrap">
	<div class="ibk-stuffbox" style="margin-top: 50px;">
		<h3 class="ibk-h3">Server Stage</h3>
		<div class="inside">
			<div class="ibk-inside-item"> 
				<div class="row">
					<div class="col-xs-12">
						<div class="ibk-view-system-wrap">
							<div class="ibk-view-system-message"><strong>WordPress Version:</strong> <?php echo $wp_version;?></div>
							<?php $icon = ($wp_version) ? 'ok-sign' : 'alert'; ?>
							<div class="ibk-view-system-icon"><span class="glyphicon glyphicon-<?php echo $icon;?>" aria-hidden="true"></span></div>
						</div>
						<div class="ibk-view-system-wrap">
							<div class="ibk-view-system-message">WordPress Memory Limit: <?php echo WP_MEMORY_LIMIT;?></div>
							<?php $icon = (WP_MEMORY_LIMIT>64) ? 'ok-sign' : 'alert';?>
							<div class="ibk-view-system-icon"><span class="glyphicon glyphicon-<?php echo $icon;?>" aria-hidden="true"></span></div>
						</div>
						<div class="ibk-view-system-wrap">
							<div class="ibk-view-system-message">WP_CRON: <?php echo $wp_cron;?></div>
							<?php $icon = ($wp_cron=='Enabled') ? 'ok-sign' : 'alert';?>
							<div class="ibk-view-system-icon"><span class="glyphicon glyphicon-<?php echo $icon;?>" aria-hidden="true"></span></div>
						</div>
						<div class="ibk-view-system-wrap">
							<div class="ibk-view-system-message">WP Plugin Diretory: <?php echo $plugin_dir;?></div>
							<?php $icon = ($plugin_dir && is_writable($plugin_dir)) ? 'ok-sign' : 'alert';?>
							<div class="ibk-view-system-icon"><span class="glyphicon glyphicon-<?php echo $icon;?>" aria-hidden="true"></span></div>
						</div>
						<div class="ibk-view-system-wrap">
							<div class="ibk-view-system-message">WP Themes Directory: <?php echo $themes_dir;?></div>
							<?php $icon = ($themes_dir && is_writable($themes_dir)) ? 'ok-sign' : 'alert';?>
							<div class="ibk-view-system-icon"><span class="glyphicon glyphicon-<?php echo $icon;?>" aria-hidden="true"></span></div>
						</div>
						<div class="ibk-view-system-wrap">
							<div class="ibk-view-system-message">WP Uploads: <?php echo $uploads_dir;?></div>
							<?php $icon = ($uploads_dir && is_writable($uploads_dir)) ? 'ok-sign' : 'alert';?>
							<div class="ibk-view-system-icon"><span class="glyphicon glyphicon-<?php echo $icon;?>" aria-hidden="true"></span></div>
						</div>
						<div class="ibk-view-system-wrap">
							<div class="ibk-view-system-message">Alternative WP Cron: <?php echo $wp_alternative_cron;?></div>
							<?php $icon = ($wp_alternative_cron=='Disabled') ? 'ok-sign' : 'alert';?>
							<div class="ibk-view-system-icon"><span class="glyphicon glyphicon-<?php echo $icon;?>" aria-hidden="true"></span></div>
						</div>
						<div class="ibk-view-system-wrap">
							<div class="ibk-view-system-message"><strong>PHP Version:</strong> <?php echo $php_ver;?></div>
							<?php $icon = ($php_ver>5.2) ? 'ok-sign' : 'alert';?>
							<div class="ibk-view-system-icon"><span class="glyphicon glyphicon-<?php echo $icon;?>" aria-hidden="true"></span></div>
						</div>
						<div class="ibk-view-system-wrap">
							<div class="ibk-view-system-message">PHP Safe Mode: <?php echo $safe_mode;?></div>
							<?php $icon = ($safe_mode=='Off') ? 'ok-sign' : 'alert';?>
							<div class="ibk-view-system-icon"><span class="glyphicon glyphicon-<?php echo $icon;?>" aria-hidden="true"></span></div>
						</div>
						<div class="ibk-view-system-wrap">
							<div class="ibk-view-system-message">PHP Memory Limit: <?php echo $memory_limit;?></div>
							<?php $icon = ($memory_limit>=256) ? 'ok-sign' : 'alert';?>
							<div class="ibk-view-system-icon"><span class="glyphicon glyphicon-<?php echo $icon;?>" aria-hidden="true"></span></div>
						</div>
						<div class="ibk-view-system-wrap">
							<div class="ibk-view-system-message">PHP Zip: <?php echo $php_zip;?></div>
							<?php $icon = ($php_zip=='On') ? 'ok-sign' : 'alert';?>
							<div class="ibk-view-system-icon"><span class="glyphicon glyphicon-<?php echo $icon;?>" aria-hidden="true"></span></div>
						</div>
						<div class="ibk-view-system-wrap">
							<div class="ibk-view-system-message">PHP RecursiveIterator Class: <?php echo $recursive_iterator;?></div>
							<?php $icon = ($recursive_iterator=='On') ? 'ok-sign' : 'alert';?>
							<div class="ibk-view-system-icon"><span class="glyphicon glyphicon-<?php echo $icon;?>" aria-hidden="true"></span></div>
						</div>						
						<div class="ibk-view-system-wrap">
							<div class="ibk-view-system-message">PHP SplFileObject Class: <?php echo $spl_file_object;?></div>
							<?php $icon = ($spl_file_object=='On') ? 'ok-sign' : 'alert';?>
							<div class="ibk-view-system-icon"><span class="glyphicon glyphicon-<?php echo $icon;?>" aria-hidden="true"></span></div>
						</div>
						<div class="ibk-view-system-wrap">
							<div class="ibk-view-system-message">PHP Max Execution Time: <?php echo $max_execution_time;?></div>
							<?php $icon = ($max_execution_time>300) ? 'ok-sign' : 'alert';?>
							<div class="ibk-view-system-icon"><span class="glyphicon glyphicon-<?php echo $icon;?>" aria-hidden="true"></span></div>
						</div>
						<div class="ibk-view-system-wrap">
							<div class="ibk-view-system-message">OpenSSL: <?php echo $open_ssl;?></div>
							<?php $icon = ($open_ssl=='On') ? 'ok-sign' : 'alert';?>
							<div class="ibk-view-system-icon"><span class="glyphicon glyphicon-<?php echo $icon;?>" aria-hidden="true"></span></div>
						</div>
						<div class="ibk-view-system-wrap">
							<div class="ibk-view-system-message">cURL: <?php echo $curl;?></div>
							<?php $icon = ($curl=='On') ? 'ok-sign' : 'alert';?>
							<div class="ibk-view-system-icon"><span class="glyphicon glyphicon-<?php echo $icon;?>" aria-hidden="true"></span></div>
						</div>
						<div class="ibk-view-system-wrap">
							<div class="ibk-view-system-message"><strong>MySQL version:</strong> <?php echo $mysql_ver;?></div>
							<?php $icon = ($mysql_ver>5) ? 'ok-sign' : 'alert';?>
							<div class="ibk-view-system-icon"><span class="glyphicon glyphicon-<?php echo $icon;?>" aria-hidden="true"></span></div>
						</div>
					</div>
				</div>
			</div>		
		</div>
	</div>
</div>		
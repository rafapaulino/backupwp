<?php 
if (isset($_GET['subtab']) && $_GET['subtab']){
	$id = false;
	if (isset($_GET['id']) && $_GET['id']){
		$id = $_GET['id'];
	}
	$meta_arr = ibk_return_metas_from_custom_db('backups', $id);//func available in utilities.php
	?>
	<script>
		var ibk_wp_db_prefix = "<?php global $wpdb;echo $wpdb->prefix;?>";
	</script>
		<form action="<?php echo $url.'&tab=manage_backups';?>" method="post">
			<?php 
				if ($id){
					?>
					<input type="hidden" value="<?php echo $id;?>" name="id" />
					<?php 	
				}
			?>
			<div class="ibk-stuffbox" style="margin-top: 50px;">
				<h3 class="ibk-h3">Add/Edit Snapshot</h3>
				<div class="inside">
					<div class="ibk-inside-item"> 
						<div class="input-group input-group-lg">
  							<span class="input-group-addon" id="basic-addon1">Snapshot Name</span>
 							 <input type="text" class="form-control" placeholder="My Backup" name="name" value="<?php echo $meta_arr['name'];?>" aria-describedby="basic-addon1">
						</div>
					</div>
					<div class="ibk-inside-item">
						<div class="row">
							<div class="col-xs-6">
								<div class="form-group">
									<label class="control-label">Description :</label>
									<textarea name="description" class="form-control text-area" cols="30" rows="5" placeholder="Some details..."><?php echo $meta_arr['description'];?></textarea>
								</div>
							</div>
						</div>
					</div>
					
					<div class="ibk-line-break"></div>
					
					<div class="ibk-inside-item"> 
						<h3>Files to Backup</h3>
						<p>Select whicth files should be included into the Snapshot</p>
						<div class="btn-group" data-toggle="buttons" style="margin:10px 0 15px 0">
						<?php 
								$arr = array(
												'all'=>'All',
												'custom'=>'Custom',
												'none'=>'None',
											);
								foreach ($arr as $k=>$v){
									?>
										<label class="btn btn-primary btn-info <?php if ($meta_arr['save_files']==$k) echo 'active';?> ">
											<?php $checked = ($meta_arr['save_files']==$k) ? 'checked' : '';?>
											<input type="radio" name="save_files" <?php echo $checked;?> id="<?php echo $k;?>" value="<?php echo $k;?>"  onChange="indeed_select_show_div(this, 'custom', '#ibk-save_files-custom_option');"> <?php echo $v;?>
										</label>
									<?php 	
								}
							?>		
							
						</div>					
							
					
					<?php $display = ($meta_arr['save_files']=='custom') ? 'block' : 'none';?>
					<div id="ibk-save_files-custom_option" style="display: <?php echo $display;?>;">
							<?php 
									$arr = array(
													'themes' => 'Themes',
													'plugins' => 'Plugins',
													'uploads' => 'Media Files',
													'wp-config.php' => 'wp-config.php',	
												);
									foreach ($arr as $k=>$v){
										?>
										<label class="checkbox-inline ibk-checkbox-wrap"><input type="checkbox" onClick="ibk_make_inputh_string(this, '<?php echo $k;?>', '#save_files_list');" <?php if (strpos($meta_arr['save_files_list'], $k)!==FALSE ) echo 'checked';?>/><?php echo $v;?></label>
										<?php 	
									}
								?>
								<input type="hidden" value="<?php echo $meta_arr['save_files_list'];?>" name="save_files_list" id="save_files_list" />
					</div>	
					</div>
					<div class="ibk-inside-item">
						<div class="row">
							<div class="col-xs-4">
								<div class="form-group">
								<label class="control-label">Excluded Files:</label>
								<textarea name="excluded_files" class="form-control text-area" placeholder="exemple.php,image.png,style.css..."><?php echo $meta_arr['excluded_files'];?></textarea>
								</div>
							</div> 
						</div>	
					</div>	
					
					<div class="ibk-line-break"></div>
					
					<div class="ibk-inside-item"> 
						<h3>DataBase to Backup</h3>
						<p>Pick Up all the Tables or just some of them and exclude those that are not necessary to be included into this Snapshot</p>
						<div class="row">
							<div class="col-xs-4">
								<div class="form-group">
								<label class="control-label">Tables</label>
								<select class="form-control m-bot15" onChange="ibk_write_tag_value(this, '#save_db_table_list', '#ibk-database-list-tables', 'backup-t-items-');">
									<option value="0">...</option>
									<option value="-1">None</option>
									<?php $selected = ($meta_arr['save_db_table_list']===FALSE) ? 'selected' : '';?>									
									<option value="all" <?php echo $selected;?> >All Tables</option>
									<option value="wp">+ all WP Native Tables</option>
									<option value="non_wp">+ all Non-WP Tables</option>
								</select>
								<?php 
									if ($meta_arr['save_db_table_list']===FALSE){
										$meta_arr['save_db_table_list'] = ibk_get_table_list();										
										$meta_arr['save_db_table_list'] = implode(',', array_keys($meta_arr['save_db_table_list']) );
									}
								?>
								<input type="hidden" id="save_db_table_list" name="save_db_table_list" value="<?php echo $meta_arr['save_db_table_list'];?>" />
								</div>
							</div> 
							</div>
							<div id="ibk-database-list-tables"><?php 
							if ($meta_arr['save_db_table_list']){
								$table_names = ibk_get_table_list();
								$items = explode(',', $meta_arr['save_db_table_list']);
								foreach ($items as $v){
									?>
										<div id="<?php echo "backup-t-items-" . $v;?>" class="ibk-tag-item"><?php
										echo $table_names[$v];
										?><div class="ibk-remove-tag" onclick="ibk_remove_db_tag('<?php echo $v;?>', '#backup-t-items-', '#save_db_table_list');" title="Removing tag">x</div>
										</div>									
									<?php 
								}
							}
						?></div>	
					</div>	
					
					<div class="ibk-line-break"></div>
					
					<div class="ibk-inside-item"> 
						<h3>When to BackUp</h3>
						<p>The Snapshot can run instantly, on a specific date or periodically.</p>
						<div class="btn-group" data-toggle="buttons" style="margin:10px 0 15px 0">
						<?php 
								$arr = array(
												'0' => 'Right Now',
												'-1' => 'Scheduled',
												'1' => 'Periodically',
											);
								foreach ($arr as $k=>$v){
									?>
										<label class="btn btn-primary btn-info <?php if ($meta_arr['backup_interval_type']==$k) echo 'active';?> ">
											<?php $checked = ($meta_arr['backup_interval_type']==$k) ? 'checked' : '';?>
											<input type="radio" name="backup_interval_type" id="<?php echo $k;?>" value="<?php echo $k;?>" <?php echo $checked;?>  onChange="ibk_backup_interval(this.value);" > <?php echo $v;?>
										</label>
									<?php 	
								}
							?>		
							
						</div>		
					</div>
					<?php $display = ($meta_arr['backup_interval_type']==-1) ? 'block' : 'none';?>
					<div class="ibk-inside-item" id="cron-specified_date" style="display: <?php echo $display;?>;" >
						<div class="row">
							<div class="col-xs-3">
								<div class="input-group">
  									<span class="input-group-addon" id="basic-addon1">Date</span>
 							 		<input type="text" class="form-control" placeholder="pick a date" id="specified_date" name="cron-specified_date" value="<?php echo $meta_arr['specified_date'];?>" aria-describedby="basic-addon1">
								</div>		
							</div>
						</div>	
					</div>
					<?php $display = ($meta_arr['backup_interval_type']==1) ? 'block' : 'none';?>
					<div class="ibk-inside-item" id="cron-periodically" style="display: <?php echo $display;?>">
						<div class="row">
							<div class="col-xs-3">
								<div class="form-group">
									<select name="cron-periodically" class="form-control m-bot15" >
										<?php 
											$arr = array(
															'0.25' => 'Every 15 minutes',
															'1' => 'On every hour',
															'12' => 'On every 12 hours',
															'24' => 'Once a day',	
															'168' => 'Once a week',
															'720' => 'Once a month',
														);
											foreach ($arr as $k=>$v){
												?>
												<option value="<?php echo $k;?>" <?php if ($meta_arr['cron-periodically']==$k) echo 'selected';?> ><?php echo $v;?></option>
												<?php 	
											}
										?>
									</select>		
								</div>
							</div>
						</div>			
					</div>
					
					<div class="ibk-line-break"></div>
					
					<div class="ibk-inside-item"> 
						<h4>History Versions</h4>
						<div class="row">
							<div class="col-xs-3">
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon1">Max</span>
									<input type="number" class="form-control" min="1" name="max_archives" value="<?php echo $meta_arr['max_archives'];?>" />
								</div>		
							</div>
						</div>	
					</div>		
					
					<div class="ibk-line-break"></div>
										
					<div class="ibk-inside-item"> 
					<h3>Snapshot Destination</h3>
						<p>Select one of your preset Destination</p>
						<div class="row">
							<div class="col-xs-3">
								<div class="form-group">
									<?php
									$data = $this->ibk_get_items_list('destinations', 'ASC', 0);
									if ($data){?>
									<select class="form-control m-bot15" name="destination"><?php 
										foreach ($data as $obj){
											?>
												<option value="<?php echo $obj->id;?>" <?php if ($meta_arr['destination']==$obj->id)echo 'selected';?> ><?php echo $obj->name;?></option>
											<?php 	
										}
									?></select>								
									<?php } else {?>
										No destinations available. <a href="<?php echo $url.'&tab=destinations&subtab=edit_create'?>">Make One!</a>
									<?php } ?>
								</div>		
							</div>
						</div>
						
					</div>	
					
					<div class="ibk-line-break"></div>
					
					<div class="ibk-inside-item"> 
					<h3>Snapshot Color</h3>
						<div>
							<?php 
								$this->ibk_get_colors_for_admin_boxes($meta_arr['admin_box_color']);//print the select color for box 
							?>
						</div>
					</div>					
					<div class="ibk-line-break"></div>
					<div class="ibk-bttn-wrapp"> 
						<?php
							$bttn = "Create";
							if ($_GET['subtab']=='edit') $bttn = "Update";
						?>
						 <input type="submit" value="<?php echo $bttn;?>" name="save-bttn" class="button button-primary button-large"/>
					</div>	
																																		
				</div>
			</div>		
		</form>
	<?php 	
} else {
	/***************************  LISTING  *************************/
	?>
		<div>
			<a href="<?php echo $url.'&tab=manage_backups&subtab=add_new'?>" class="ibk-add-new"><i title="" class="fa-ibk fa-add-backup-ibk"></i><span>Add SnapShot</span></a>
			<span class="ibk-top-message">...create your Snapshot and personalize it!</span>
		</div>
		<div class="ibk-backup-items-wrap">
	<?php 
	/************* create/edit ************/
	if (isset($_POST['save-bttn'])){
 		$this->ibk_save_update_backup_item($_POST);	
	}
	
	/************** LIST *****************/
	$data = $this->ibk_get_items_list('backup');
	if ($data ){
		foreach ($data as $obj){
			$meta = ibk_return_metas_from_custom_db('backups',$obj->id);//func available in utilities.php
			$this->ibk_create_admin_backup_box($obj->id, $meta, $url);
		}	
	}else{ ?>
		<div class="ibk-nodata-wrapper">
			<img src="<?php echo IBK_URL;?>admin/assets/images/nosnapshots.png"/>
			<a href="<?php echo $url.'&tab=manage_backups&subtab=add_new'?>" class="ibk-add-new"><i title="" class="fa-ibk fa-add-backup-ibk"></i><span>Add SnapShot</span></a>
		</div>
	<?php } ?>
		<div class="clear"></div>
		</div>
	<?php 
}


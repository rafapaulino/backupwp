<?php 
if (!class_exists('IndeedDoBackup')){
	class IndeedDoBackup{
		private $backup_id = FALSE;
		private $backup_metas = array();
		private $destination_metas = array();
		private $general_metas = array();
		private $filename = '';
		private $temp_dir_sql = '';
		private $created_zip_date = FALSE;
		private $log_object;
		
		public function __construct($id){
			/*
			 * @param id of Snapshot(backup) Item 
			 * @return none
			 */
			if (!function_exists('ibk_return_metas_from_custom_db')){
				require_once IBK_PATH . 'utilities.php';
			}
			if (!class_exists('IndeedDoLogs')){
				require_once IBK_PATH . 'classes/IndeedDoLogs.class.php';
			}

			$this->created_zip_date = time();	
			$this->backup_id = $id;//setting the backup id
			$this->filename = 'superbackup_' . md5('superbackup_indeed') . '_' . $this->backup_id . '_' . $this->created_zip_date . '.zip';//ZIP FILENAME
			$this->init_backup_metas();//get and set backup metas
			$this->init_destination_metas();//get and set destination metas
			$this->init_general_metas();//set the general metas
			
			$this->set_memory_limit();
			
			$this->send_email(1);//send e-mail process start
			
			$this->log_object = new IndeedDoLogs();//Logs Object
			$this->log_object->set_type('backup');
			$this->log_object->set_action_id($this->backup_id);
			$this->log_object->create_log('start', 'Process Start!', 1);		
			
			$this->backup_db();//save sql stuff
			$this->backup_files();//make zip file	
			$this->delete_temporary_files_folders();
			$send = $this->move_file();//send file 
			
			$this->set_cron();//set next cron job
			if ($send){
				//process finish ok
				$this->log_object->insert_log('finish', 'Process Finish!', 1);
				$removed_instance = $this->check_version_limit();				
				$this->write_log_snapshot_file($removed_instance);
			} else {
				//error sending file to destination...
				$this->log_object->insert_log('finish', 'Process Finish but file was not send. Error!', 2);
			}
			
			$this->send_email(2);//send e-mail process email
		}

		private function backup_files(){
			/*
			 * add files and folders to zip file
			 * @param none
			 * @return none
			 */
			try {
				$dirs = FALSE;
				if ($this->backup_metas['save_files'] == 'all') {
					$dirs = array(
									'themes',
									'plugins',
									'uploads',
									'wp-config.php',
					);
				} elseif ($this->backup_metas['save_files']=='custom'){
					$dirs = explode(',', $this->backup_metas['save_files_list']);
				}
				//sql table
				if ($this->temp_dir_sql){
					// put the sql files into zip
					$this->make_zip($this->temp_dir_sql, WP_CONTENT_DIR . '/uploads/'. $this->general_metas['ibk_backup_dir'] . '/' . $this->filename, true, array());
				}
				
				$excluded_folders = array(
											WP_CONTENT_DIR . '/uploads/indeed-backups', 
											WP_CONTENT_DIR . '/uploads/'. $this->general_metas['ibk_backup_dir']
										 );
				$excluded_files = explode(',', $this->backup_metas['excluded_files']);
				foreach ($dirs as $dir){					
					//set the path to the target dir
					if ($dir=='wp-config.php'){
						$dir = ABSPATH . '/wp-config.php';
					} else {
						$dir = WP_CONTENT_DIR . '/' . $dir;
					}
					
					$this->make_zip($dir, WP_CONTENT_DIR . '/uploads/'. $this->general_metas['ibk_backup_dir'] . '/' . $this->filename, true, $excluded_folders, $excluded_files);
				}
				$this->log_object->insert_log('zip', 'Backup Files Completed', 1);				
			} catch (Exception $e){
				$msg = 'Unable to Backup Files. ' . $e->getMessage();
				$this->log_object->insert_log('zip', $msg, 1);
				$this->send_email(3, $msg);//send the error via e-mail
			}
		}
		
		private function backup_db(){
			/*
			 * iterate through database tables and save them
			 * @param none
			 * @return none
			 */
			try{
				if (!empty($this->backup_metas['save_db_table_list'])){
					$this->create_temp_dir();
					$data = explode(',', $this->backup_metas['save_db_table_list']);
					foreach ($data as $table){						
						$this->write_sql_file($table);
					}
				}
				$this->log_object->insert_log('sql', 'Backup Database Completed', 1);				
			} catch (Exception $e){
				$msg = 'Unable to Backup Database. ' . $e->getMessage();
				$this->log_object->insert_log('sql', $msg, 1);
				$this->send_email(3, $msg);//send the error via e-mail
			}
		}
		
		private function create_temp_dir(){
			/*
			 * Create temporary folder to hold sql files
			 * @param none
			 * @return none
			 */
			$file = WP_CONTENT_DIR . '/uploads/'. $this->general_metas['ibk_backup_dir'] . '/indeed_' . $this->backup_id . '_' . $this->created_zip_date;
			if (!file_exists($file)){
				try {
					mkdir($file);
				} catch (Exception $e){
					$msg = 'Failed to create temporary folder: ' . $file . ', '. $e->getMessage();
					$this->log_object->insert_log('sql', $msg, 2);
					$this->send_email(3, $msg);//send the error via e-mail
				}				
			}	
			$this->temp_dir_sql =  $file . '/sql';
			if (!file_exists($this->temp_dir_sql)){
				try {
					mkdir($this->temp_dir_sql);
				} catch (Exception $e){
					$msg = 'Failed to create temporary folder: ' . $this->temp_dir_sql . ', '. $e->getMessage();
					$this->log_object->insert_log('sql', $msg, 2);
					$this->send_email(3, $msg);//send the error via e-mail
				}
			}
		}

		private function delete_folder($target){
			/*
			 * @param none
			 * @return none
			 */			
			if (file_exists($target)){
				$this->indeed_rmdir_recursive($target);
			}		
		}
		
		private function delete_temporary_files_folders(){
			/*
			 * @param none
			 * @return none
			 */
			$path = WP_CONTENT_DIR . '/uploads/'. $this->general_metas['ibk_backup_dir'] . '/indeed_' . $this->backup_id . '_' . $this->created_zip_date;
			if (file_exists($path)){
				$this->indeed_rmdir_recursive($path);
			}
		}
		
		private function indeed_rmdir_recursive($dir) {
			/*
			 * delete a directory with all files and folders that contains
			 * @param target directory to delete
			 * @return none
			 */
			try {
				foreach (scandir($dir) as $file) {
					if ('.' === $file || '..' === $file){
						continue;
					}
					if (is_dir("$dir/$file")){
						$this->indeed_rmdir_recursive("$dir/$file");
					}
					else {
						unlink("$dir/$file");
					}
				}
				rmdir($dir);				
			} catch (Exception $e){
				$msg = 'Unable to delete file or folder. ' . $e->getMessage();
				$this->log_object->insert_log('', $msg, 2);
				$this->send_email(3, $msg);//send the error via e-mail
			}

		}
		
		private function init_backup_metas(){
			/*
			 * set backup metas
			 * @param none
			 * @return none
			 */
			if (!empty($this->backup_id)){
				try{
					$this->backup_metas = ibk_return_metas_from_custom_db('backups', $this->backup_id);
				} catch (Exception $e){
					$msg = 'Unable to initiate Backup Metas. '. $e->getMessage();
					$this->log_object->insert_log('start', $msg, 2);
					$this->send_email(3, $msg);//send the error via e-mail
				}					
			}
		}
		
		private function init_destination_metas(){
			/*
			 * set destination metas
			 * @param none
			 * @return none
			 */
			if (!empty($this->backup_metas['destination'])){
				try{
					$this->destination_metas = ibk_return_metas_from_custom_db('destinations', $this->backup_metas['destination']);
				} catch (Exception $e){
					$msg = 'Unable to initiate Destination Metas. '. $e->getMessage();
					$this->log_object->insert_log('start', $msg, 2);
					$this->send_email(3, $msg);//send the error via e-mail
				}						
			}		
		}
		
		private function init_general_metas(){
			/*
			 * @param none
			 * @return none
			 */
			$this->general_metas = ibk_get_general_metas();
		}
		
		private function move_file(){
			/*
			 * function that send zip file to destination
			 * @param none
			 * @return none
			 */
			$file = WP_CONTENT_DIR . '/uploads/'. $this->general_metas['ibk_backup_dir'] . '/' . $this->filename;
			switch ($this->destination_metas['type']){
				case 'local':
					try {
						//destination dir exists??
						if (!file_exists($this->destination_metas['local_folder_target'])) {
							mkdir($this->destination_metas['local_folder_target'], 0777, true);
						}
						//move file to destination dir
						rename( $file, $this->destination_metas['local_folder_target'] . $this->filename );
						$this->log_object->insert_log('sending_file', 'Backup File '.$this->filename.' moved to Local Destination!', 1);
						return TRUE;
					} catch (Exception $e){
						$msg = 'Failed to move Backup File '.$this->filename.' to Local Destination. ' . $e->getMessage();
						$this->log_object->insert_log('sending_file', $msg, 2);
						$this->send_email(3, $msg);//send the error via e-mail						
					}					
					return FALSE;
				break;
				case 'google':
					try {
						if (!class_exists('IndeedGoogle')){
							require_once IBK_PATH . 'classes/API/IndeedGoogle.class.php';
						}						
						$goo = new IndeedGoogle($this->backup_metas['destination']);
						$response = $goo->send_file( $file );
						if ($response===TRUE){
							$msg = 'Backup File '.$this->filename.' was sent to Google Drive!';
							$this->log_object->insert_log('sending_file', $msg, 1);							
							$this->delete_zip_file($file);
							return TRUE;
						} else {
							//we have an error
							if (is_array($response) && count($response)) {
								//write the error in logs 
								$msg = implode("\n", $reponse);
								$this->log_object->insert_log('sending_file', $msg, 2);																					
							}
						} 
						unset($goo);						
					} catch (Exception $e){
						$msg = 'Fail sending Backup File '.$this->filename.' to Google Drive. ' . $e->getMessage();
						$this->log_object->insert_log('sending_file', $msg, 2);
						$this->send_email(3, $msg);//send the error via e-mail						
					}
					return FALSE;
				break;
				case 'ftp':
					try {
						if (!class_exists('IndeedFtp')){
							require_once IBK_PATH . 'classes/API/IndeedFtp.class.php';
						}						
						$ftp = new IndeedFtp($this->backup_metas['destination']);
						$ftp->login();
						$response = $ftp->send_file( $file );

						if ($response===TRUE){
							$this->log_object->insert_log('sending_file', 'Backup File '.$this->filename.' was sent to FTP!', 1);
							$this->delete_zip_file($file);
							return TRUE;
						} else {
							//we have an error
							if (is_array($response) && count($response)) {
								//write the error in logs 
								$msg = implode("\n", $reponse);
								$this->log_object->insert_log('sending_file', $msg, 2);
							}
						}						
						$ftp->logout();
					} catch (Exception $e){
						$msg = 'Fail sending Backup File '.$this->filename.' to FTP. ' . $e->getMessage();
						$this->log_object->insert_log('sending_file', $msg, 2);
						$this->send_email(3, $msg);//send the error via e-mail
					}
					return FALSE;
				break;
				case 'dropbox':
					try {
						if (!class_exists('IndeedDropbox')){
							require_once IBK_PATH . 'classes/API/IndeedDropbox.class.php';
						}
						$dropbox_obj = new IndeedDropbox($this->backup_metas['destination']);
						$dropbox_obj->login();
						$dropbox_obj->send_file($file, basename($file));
						$msg = 'Backup File '.$this->filename.' was sent to Dropbox!';
						$this->log_object->insert_log('sending_file', $msg, 1);
						$this->delete_zip_file($file);
						return TRUE;
					} catch (Exception $e){
						$msg = 'Fail sending Backup File '.$this->filename.' to Dropbox. ' . $e->getMessage();
						$this->log_object->insert_log('sending_file', $msg, 2);
						$this->send_email(3, $msg);//send the error via e-mail
					}
					return FALSE;
				break;
				case 'rackspace':		
					try {
						require_once IBK_PATH . 'classes/API/IndeedRackSpace.class.php';						
						$obj = new IndeedRackSpace($this->backup_metas['destination']);
						$obj->send_file( $file );
						return TRUE;
					} catch (Exception $e){
						$msg = 'Fail sending Backup File '.$this->filename.' to RackSpace. ' . $e->getMessage();
						$this->log_object->insert_log('sending_file', $msg, 2);
						$this->send_email(3, $msg);//send the error via e-mail						
					}	
					return FALSE;
				break;
				case 'amazon':
					try {
						if (!class_exists('IndeedAmazonS3')){
							require_once IBK_PATH . 'classes/API/IndeedAmazonS3.class.php';
						}
						$obj = new IndeedAmazonS3($this->backup_metas['destination']);
						$send = $obj->send_file($file);
						if ($send) return TRUE;
					} catch (Exception $e){
						$msg = 'Fail sending Backup File '.$this->filename.' to Amazon S3. ' . $e->getMessage();
						$this->log_object->insert_log('sending_file', $msg, 2);
						$this->send_email(3, $msg);//send the error via e-mail						
					}
					return FALSE;
				break;
			}
			
		}

		private function send_email($type, $msg=FALSE){
			/*
			 * @param type int, msg string
			 * 1 - process start
			 * 2 - process end
			 * 3 - error occured
			 * use msg for errors
			 * @return none
			 */
			if (!$this->general_metas['ibk_email_sent']) return;
			
			$do = FALSE;
			if ($type==1){
				//process start
				if ($this->general_metas['ibk_email_sent_2']){
					$do = true;
					$subject = 'New process started!';
					$msg = 'Process start at ' . date("Y-m-d H:i:s", $this->created_zip_date);
				}
			} elseif ($type==2){
				//process end
				if ($this->general_metas['ibk_email_sent_1']){
					$do = true;
					$subject = 'Process end!';
					$msg = 'Process end at ' . date("Y-m-d H:i:s", time());
				}
			} else {
				if ($this->general_metas['ibk_email_sent_3']){
					$do = true;
					$subject = 'Process ends, unexpected error!';
					if (!$msg) $msg = 'An unexpected error has occurred!';
				}
			}
			if ($do){
				$to = $this->general_metas['ibk_email'];
				wp_mail( $to, $subject, $msg );				
			}		
		}
		
		private function delete_zip_file($file){
			/*
			 * delete zip file
			 * @param zip file full path 
			 * @return none
			 */
			if (!$this->general_metas['ibk_backup_files']){
				try {
					if (file_exists($file)){
						unlink($file);
						$this->log_object->insert_log('delete_zip', 'Delete temporary file: ' . $file , 1);
					}
				} catch (Exception $e){
					$msg = 'Unable to delete: ' . $file . '. '. $e->getMessage();
					$this->log_object->insert_log('delete_zip', $msg, 2);
					$this->send_email(3, $msg);//send the error via e-mail
				}
			}
		}		
		
		private function set_cron(){
			/*
			 * if it's case set the cron for next backup
			 */
			if ($this->backup_metas['backup_interval_type']==1 && isset($this->backup_metas['cron-periodically'])){
				$time = time() + ($this->backup_metas['cron-periodically']*60*60);
				wp_schedule_single_event( $time , 'indeed_main_job', array( $this->backup_id ) );
			}			
		}
		
		private function make_zip($sources, $destination, $include_dir = false, $excluded_folders=array(), $excluded_files=FALSE ){
			/*
			 * make zip archive 
			 * @params
			 * sources - must be array full path of files or folders
			 * destination - zip file , full path
			 * include_dir boolean, if include the parent dir
			 * excluded_files - array of excluded files, must be full path 
			 * @return none
			 */
			if (!extension_loaded('zip')) {
				$this->log_object->insert_log('zip', 'ZipArchive Class does not exists!', 2);
				return false;
			}
		
			try{
				$zip = new ZipArchive();
				if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
					return false;
				}				
			} catch (Exception $e){
				$msg = 'Unable to create zip file! ' . $e->getMessage();
				$this->log_object->insert_log('zip', $msg, 2);
				$this->send_email(3, $msg);//send the error via e-mail
			}
			
			if (!is_array($sources)){
				$sources = array($sources);
			}
		
			try {
				foreach ($sources as $source ){
					$source = str_replace('\\', '/', realpath($source));
				
					if (is_dir($source) === true){
						$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);
				
						if ($include_dir) {
							$arr = explode("/",$source);
							$maindir = $arr[count($arr)- 1];
							$source = "";
							for ($i=0; $i < count($arr) - 1; $i++) {
								$source .= '/' . $arr[$i];
							}
							$source = substr($source, 1);
							$zip->addEmptyDir($maindir);
						}
							
						foreach ($files as $file){
							$file = str_replace('\\', '/', $file);
							if ( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) ) continue;
							$file = realpath($file);
				
							$zip_it = true;
							foreach ($excluded_folders as $excluded_folder){
								if (strpos($file, $excluded_folder)!==FALSE){
									$zip_it = false;
								}
							}
				
							if ($zip_it){
								if (is_dir($file) === true){
									$zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
								}
								else if (is_file($file) === true){
									if ($excluded_files && in_array($file, $excluded_files)){
										continue;
									}
									$zip->addFile($file, str_replace($source . '/', '', $file));
								}
							}
						}
					} elseif (is_file($source) === true){
						$zip->addFromString(basename($source), file_get_contents($source));
					}
				}				
			} catch (Exception $e){
				$msg = 'Unable to add file to zip archive! ' . $e->getMessage();
				$this->log_object->insert_log('zip', $msg, 2);
				$this->send_email(3, $msg);//send the error via e-mail
			}
			return $zip->close();
		}
		
		private function write_sql_file($table_name){
			/*
			 * copy mysql table and store into a file
			 * @param target database table, sql dir
			 * @return none
			 */
			global $wpdb;
			$table_name = str_replace($wpdb->prefix, '', $table_name);
			$filename = $this->temp_dir_sql . '/' . $table_name .'.sql';
			try {
				$file = fopen($filename, 'w');
			} catch (Exception $e){
				$msg = 'Unable to create sql file: ' . $filename . '. ' . $e->getMessage();
				$this->log_object->insert_log('sql', $msg, 2);
				$this->send_email(3, $msg);//send the error via e-mail
			}
						
			try {				
				/*********** create table ***********/
				$temp_table_name = $table_name.'_indeed_temp';
				$q = 'SHOW CREATE TABLE ' . $wpdb->prefix . $table_name;
				$data = $wpdb->get_results($q);
				$table = (array)$data[0];
				$table['Create Table'] = str_replace('CREATE TABLE `' . $wpdb->prefix . $table_name . '`', 'CREATE TABLE IF NOT EXISTS `' . $temp_table_name . '`', $table['Create Table']);
				$create_str = str_replace("\n", "", $table['Create Table']) . "\n";
				fwrite($file, $create_str);
				fwrite($file, "TRUNCATE TABLE `" . $temp_table_name . "`;\n");
				/*********** insert values into table ************/
				$start_limit = 0;
				$limit_step = (empty($this->general_metas['ibk_db_segmentation'])) ? 100 : $this->general_metas['ibk_db_segmentation'];
				$loop = TRUE;
				$str = '';
				while ($loop){
					$q = "SELECT * FROM " . $wpdb->prefix . $table_name . " LIMIT $start_limit, $limit_step";
					$data = $wpdb->get_results($q);
					if ($data){
						$str = '';
						$subarr = FALSE;
						foreach ($data as $table_row){
							foreach ($table_row as $k=>$v){
								$arr[] = "'" . mysql_real_escape_string(trim($v)) . "'";
							}
							$substring = '';
							$substring .= "(";
							$substring .= implode(",", $arr);
							$substring .= ")";
							$subarr[] = $substring;
							unset($arr);
						}
						$str .= "INSERT INTO `" . $temp_table_name . "` VALUES";
						$str .= implode(",", $subarr);
						$str .= ";\n";
						fwrite($file, $str);
						$loop = TRUE;
					} else {
						$loop = FALSE;
					}
					$start_limit = $start_limit + $limit_step;
				}		
				fclose($file);				
			} catch (Exception $e){
				$msg = 'Unable to save table: ' . $table_name . '. ' . $e->getMessage();
				$this->log_object->insert_log('sql', $msg, 2);
				$this->send_email(3, $msg);//send the error via e-mail
			}
		}

		private function set_memory_limit(){
			if ($this->general_metas['ibk_memory_limit']){
				$this->general_metas['ibk_memory_limit'] = preg_replace('/\D/', '', $this->general_metas['ibk_memory_limit']);//remove characters from string
				$limit = $this->general_metas['ibk_memory_limit'] . 'M';//put the M in limit
				ini_set('memory_limit', $limit);
			}
		}
		
		private function check_version_limit(){
			/*
			 * use this for 'History Versions' available for each snapshot,
			 * @param none
			 * @return removed file
			 */
			switch ($this->destination_metas['type']){
				case 'local':			
					$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->destination_metas['local_folder_target']), RecursiveIteratorIterator::SELF_FIRST);
					$min_timestamp = time();
					foreach ($files as $file){
						$file = str_replace('\\', '/', $file);
						$file_h = basename($file);
						if (preg_match("#^superbackup(.*)$#i", $file_h)){
							//it contains indeed
							$is_zip_data = explode('.', $file_h);
							if (isset($is_zip_data[1]) && $is_zip_data[1]=='zip'){
								//it's a zip file
								$file_name_data = explode('_', $is_zip_data[0]);
								
								if ($file_name_data[2]==$this->backup_id && $file_name_data[1]==md5('superbackup_indeed') ){
									//it's a instance of our snapshot
									$snapshot_instances[$file_name_data[3]]	= $file;
									if ($file_name_data[3]<$min_timestamp){
										$min_timestamp = $file_name_data[3]; //store the minimum timestamp available
									}									
								}
							}
						}
						if (!empty($snapshot_instances) && count($snapshot_instances)>$this->backup_metas['max_archives']){
							//history has hit the limit, we must delete the first instance of this snapshot
							if (file_exists($snapshot_instances[$min_timestamp])){
								unlink($snapshot_instances[$min_timestamp]);
								return $snapshot_instances[$min_timestamp];
							}							
						}					
					}//end of foreach
				break;
				
				case 'ftp':
					if (!class_exists('IndeedFtp')){
						require_once IBK_PATH . 'classes/API/IndeedFtp.class.php';
					}
					$ftp = new IndeedFtp($this->backup_metas['destination']);//destination id
					$ftp->login();
					$snapshot_instances = $ftp->list_snapshots($this->backup_id);//snapshot id
					if (!empty($snapshot_instances) && count($snapshot_instances)>$this->backup_metas['max_archives']){
						$min_timestamp = time();
						foreach ($snapshot_instances as $k=>$v){
							if ($min_timestamp>$k){
								$min_timestamp = $k;
							}
						}
						$ftp->delete_target_file($snapshot_instances[$min_timestamp]);
						return $snapshot_instances[$min_timestamp];
					}
				break;
				
				case 'google':
					require_once IBK_PATH . 'classes/API/IndeedGoogle.class.php';				
					$goo = new IndeedGoogle($this->backup_metas['destination']);
					$goo->login();
					$data = $goo->retrieveAllFiles();
					$min_timestamp = time();
					foreach ($data as $file_obj){
						if (preg_match("#^superbackup(.*)$#i", $file_obj->title)){
							//it contains indeed
							$is_zip_data = explode('.', $file_obj->title);
							if (isset($is_zip_data[1]) && $is_zip_data[1]=='zip'){
								//it's a zip file
								$file_name_data = explode('_', $is_zip_data[0]);
								if ($file_name_data[2]==$this->backup_id && $file_name_data[1]==md5('superbackup_indeed') ){
									//it's a instance of our snapshot				
									$snapshot_instances[$file_name_data[3]]	= $file_obj->id;
									if ($file_name_data[3]<$min_timestamp){
										$min_timestamp = $file_name_data[3]; //store the minimum timestamp available
										$removed_instance = $file_obj->title;
									}
								}
							}
						}
					}
					if (!empty($snapshot_instances) && count($snapshot_instances)>$this->backup_metas['max_archives']){
						//history has hit the limit, we must delete the first instance of this snapshot
						$goo->deleteFile($snapshot_instances[$min_timestamp]);
						return $removed_instance;
					}
				break;
				
				case 'dropbox':					
					require_once IBK_PATH . 'classes/API/IndeedDropbox.class.php';
					$obj = new IndeedDropbox($this->backup_metas['destination']);
					$obj->login();
					$data = $obj->get_files();
					$min_timestamp = time();
					foreach ($data as $file){
						if (preg_match("#superbackup(.*)$#i", $file)){
							
							//it contains indeed
							$title = basename($file);
							$is_zip_data = explode('.', $title);
							if (isset($is_zip_data[1]) && $is_zip_data[1]=='zip'){
								//it's a zip file
								$file_name_data = explode('_', $is_zip_data[0]);
								if ($file_name_data[2]==$this->backup_id && $file_name_data[1]==md5('superbackup_indeed') ){
									//it's a instance of our snapshot
									$snapshot_instances[$file_name_data[3]]	= $file;
									if ($file_name_data[3]<$min_timestamp){
										$min_timestamp = $file_name_data[3]; //store the minimum timestamp available
									}
								}
							}
						}
					}
					if (!empty($snapshot_instances) && count($snapshot_instances)>$this->backup_metas['max_archives']){
						//history has hit the limit, we must delete the first instance of this snapshot
						$obj->delete_file($snapshot_instances[$min_timestamp]);
						return basename($snapshot_instances[$min_timestamp]);
					}
				break;
				
				case 'amazon':
					require_once IBK_PATH . 'classes/API/IndeedAmazonS3.class.php';
					$obj = new IndeedAmazonS3($this->backup_metas['destination']);
					$data = $obj->get_files_list();
					$min_timestamp = time();
					foreach ($data as $file){
						if (preg_match("#superbackup(.*)$#i", $file)){
							//it contains indeed
							$title = basename($file);
							$is_zip_data = explode('.', $title);
							if (isset($is_zip_data[1]) && $is_zip_data[1]=='zip'){
								//it's a zip file
								$file_name_data = explode('_', $is_zip_data[0]);
								if ($file_name_data[2]==$this->backup_id && $file_name_data[1]==md5('superbackup_indeed') ){
									//it's a instance of our snapshot
									$snapshot_instances[$file_name_data[3]]	= $file;
									if ($file_name_data[3]<$min_timestamp){
										$min_timestamp = $file_name_data[3]; //store the minimum timestamp available
									}
								}
							}
						}
					}
					if (!empty($snapshot_instances) && count($snapshot_instances)>$this->backup_metas['max_archives']){
						//history has hit the limit, we must delete the first instance of this snapshot
						$obj->delete_file($snapshot_instances[$min_timestamp]);
						return basename($snapshot_instances[$min_timestamp]);
					}
				break;	
				
			}
		}
		
		private function write_log_snapshot_file($removed_file=FALSE){
			/*
			 * use this only for ftp, google drive snapshots
			 * @param file to remove from log
			 * @return none
			 */
			//log text file (CLoud)
			if ($this->destination_metas['type']=='ftp' || $this->destination_metas['type']=='google' ||  
				$this->destination_metas['type']=='dropbox' || $this->destination_metas['type']=='amazon'){
				if (!class_exists('IndeedSnapshotText')){
					require_once IBK_PATH . 'classes/IndeedSnapshotText.class.php';
				}
				$cloud_obj = new IndeedSnapshotText($this->backup_metas['destination'], $this->backup_id);
				$cloud_obj->set_log($this->backup_metas, $this->filename, $this->general_metas['ibk_backup_dir'], $removed_file);				
			}
		}
		
		
		
		/**************** debugging *****************/
		private function write_into_debug_log($message){
			$file = IBK_PATH . 'backup_debugging.log';
			file_put_contents($file, $message, FILE_APPEND | LOCK_EX);
		}
		
	}//end of IndeedDoBackup Class
}
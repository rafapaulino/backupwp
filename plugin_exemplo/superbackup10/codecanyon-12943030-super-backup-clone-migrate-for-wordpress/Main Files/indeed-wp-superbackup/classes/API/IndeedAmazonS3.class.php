<?php 
if (!class_exists('IndeedAmazonS3')){
	class IndeedAmazonS3{
		private $metas = FALSE;
		private $destination_id = FALSE;
		private $connection = FALSE;
		
		public function __construct($id){
			/*
			 * @param id of destination
			 * @return none
			 */
			$this->destination_id = $id;
			$this->set_metas();	
			
			//load the amazon classes
			if (!class_exists('CFRuntime')){
				require_once IBK_PATH . 'classes/API/AWS/sdk.class.php';
			}
			
			//auth
			$this->login();
			
		}
		
		private function set_metas(){
			/*
			 * getting the meta values from db for current destination id and set them into $this->metas
			 * @param none
			 * @return none
			 */
			if ($this->destination_id){
				global $wpdb;
				$arr = array();
				$table_name = $wpdb->prefix . 'indeed_destination_metas';
				foreach (array('aws_key', 'aws_secret_key', 'aws_region', 'aws_bucket') as $k){ 
					$data = $wpdb->get_row('SELECT meta_value FROM ' . $table_name . ' WHERE meta_name="'.$k.'" AND destination_id='.$this->destination_id);
					if (!empty($data->meta_value)) $this->metas[$k] = $data->meta_value;
				}
			}			
		}
		
		private function login(){
			if (!empty($this->metas['aws_key']) && !empty($this->metas['aws_secret_key'])){
				$ssl = (empty($this->metas['aws_ssl'])) ? FALSE : TRUE;//use ssl?
				$this->connection = new AmazonS3(array(
						'key' => $this->metas['aws_key'],
						'secret' =>	$this->metas['aws_secret_key'],
						'certificate_authority'	=>	$ssl,
					)
				);
				$this->connection->set_region($this->metas['aws_region']);				
			}
		}
		
		public function send_file($file){
			/*
			 * @param full path of file that we want to send
			 * @return 
			 */
			$result = $this->connection->create_object($this->metas['aws_bucket'], basename($file), array('fileUpload' => $file));
			return $result;
		}
		
		public function get_file($source, $target_path){
			$result = $this->connection->get_object($this->metas['aws_bucket'], $source);
			if (!empty($result->body)){
				file_put_contents($target_path . $source, $result->body );
				return $target_path . $source;
			}
			return FALSE;			
		}
		
		public function get_files_list(){
			if ($this->connection && $this->metas['aws_bucket']){
				$data = $this->connection->list_objects($this->metas['aws_bucket']);
				if (!empty($data->body->Contents)){
					foreach ($data->body->Contents as $obj){
						$obj = (array)$obj;
						$arr[] = $obj['Key'];
					}
					return $arr;
				}				
			}
			return FALSE;
		}
		
		public function delete_file($filename){
			$this->connection->delete_object($this->metas['aws_bucket'], $filename);
		}
		
		public function get_logs_files(){
			/*
			 * search for all log files, and return them into array
			 * @param none
			 * @return array or bool
			 */
			$return_arr = FALSE;
			if ($this->connection){
				$data = $this->get_files_list();
				if (!empty($data)){
					foreach ($data as $file){
						if (preg_match("#superbackup(.*)$#i", $file)){
							$is_log = explode('.', basename($file) );
							if (isset($is_log[1]) && $is_log[1]=='log'){
								$return_arr[] = $file;
							}
						}
					}					
				}				
			}
			return $return_arr;
		}
		
		
	}//end of class IndeedAmazonS3
}
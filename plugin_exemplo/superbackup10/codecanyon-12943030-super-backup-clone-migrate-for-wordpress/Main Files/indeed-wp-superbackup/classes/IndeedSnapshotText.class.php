<?php
/*
 * Used for Cloud section
 */

if (!class_exists('IndeedSnapshotText')){
	class IndeedSnapshotText{
		private $snapshot_id = FALSE;
		private $destination_id = FALSE;
		private $destination_type = FALSE;
		private $file_name = FALSE;		
		private $temp_file = FALSE;
		private $temp_dir = FALSE;
		
		public function __construct($destinationId, $snapshotId){
			$this->destination_id = $destinationId;	
			$this->snapshot_id = $snapshotId;
			$this->destination_type = ibk_get_destination_type($destinationId);
		}
		
		public function set_log($snapshotMetas, $fileName, $temp_dir, $remove_instance=FALSE){
			/*
			 * write current snapshot log into file
			 * @param $snapshotMetas current snapshot settings, $fileName name of zip snapshot instance 
			 */
			$this->file_name = 'superbackup_' . $this->snapshot_id . '.log';//log FILENAME
			$this->temp_dir = WP_CONTENT_DIR . '/uploads/' . $temp_dir . '/';
			$this->temp_file = $this->temp_dir . $this->file_name;	
					
			$date = explode('_', $fileName);
			if (isset($date[3])){
				$last_run = str_replace('.zip', '', $date[3]);
			}
			
			$this->get_file();
			
			if (file_exists($this->temp_file)){
				$str = file_get_contents($this->temp_file);
				unlink($this->temp_file);
			} else {
				$str = FALSE;
			}
			
			if ($str){
				//adding new instances of snapshot
				$data = unserialize($str);
				$data['file_arr'][] = $fileName;
				$data['last_run'] = $last_run;
				
				//remove file $remove_instance (version limit)
				if ($remove_instance){
					//echo array_search($remove_instance, $data['file_arr']),"<br/>";
					$removed_key = array_search($remove_instance, $data['file_arr']);
					if ($removed_key!==FALSE){						
						unset($data['file_arr'][$removed_key]);
					}
				}
			} else {
				//create snapshot log
				$data = array(
								'snapshot_name' => $snapshotMetas['name'],
								'snapshot_description' => $snapshotMetas['description'],
								'admin_box_color' => $snapshotMetas['admin_box_color'],								
								'tables' => $snapshotMetas['save_db_table_list'],
								'last_run' => $last_run,
								'file_arr' => array($fileName),
							);
				if ($snapshotMetas['save_files']=='all'){
					$data['files'] = 'themes,plugins,uploads,wp-config.php';
				} else if ($snapshotMetas['save_files']=='none'){
					$data['files'] = '';
				} else {
					$data['files'] = $snapshotMetas['save_files_list'];
				}
			}
			
			//$data_for_write = json_encode($data);
			$data_for_write = serialize($data);
			
			file_put_contents($this->temp_file, $data_for_write);
			
			$this->send_file();
			unlink($this->temp_file);
		}
		
		private function get_file(){
			switch ($this->destination_type){
				case 'ftp':
					require_once IBK_PATH . 'classes/API/IndeedFtp.class.php';
					$obj = new IndeedFtp($this->destination_id);
					$obj->login();
					$file = $obj->get_log_file( $this->snapshot_id );
					$obj->copy_file_to_local($file, $this->temp_file);
				break;
				case 'google':
					//download the log file from google if exists
					require_once IBK_PATH . 'classes/API/IndeedGoogle.class.php';
					$goo = new IndeedGoogle($this->destination_id);
					$goo->login();
					$data = $goo->retrieveAllFiles();
					foreach ($data as $file_obj){
						if (preg_match("#^superbackup(.*)$#i", $file_obj->title)){
							$is_log = explode('.', $file_obj->title);
							if (isset($is_log[1]) && $is_log[1]=='log'){
								//it's a zip file
								$file_name_data = explode('_', $is_log[0]);
								if ($file_name_data[1]==$this->snapshot_id ){
									//it's a instance of our snapshot
									$target_id = $file_obj->id;
								}
							}
						}
					}
					if (!empty($target_id)){
						$file = $goo->downloadFile($target_id, $this->temp_dir);
						if ($file){
							$goo->deleteFile($target_id);
						}						
					}
				break;
				case 'dropbox':
					require_once IBK_PATH . 'classes/API/IndeedDropbox.class.php';
					$obj = new IndeedDropbox($this->destination_id);
					$obj->login();
					$files = $obj->get_files();
					foreach ($files as $file){
						if (preg_match("#superbackup(.*)$#i", $file)){
							$is_log = explode('.', $file);
							if (isset($is_log[1]) && $is_log[1]=='log'){
								//it's a zip file
								$file_name_data = explode('_', $is_log[0]);
								if ($file_name_data[1]==$this->snapshot_id ){
									//it's a instance of our snapshot
									$source_file = $file;
								}
							}
						}
					}
					if (!empty($source_file)){
						$file = $obj->get_file($source_file, $this->temp_dir);
						if ($file){
							$obj->delete_file($source_file);
						}
					}					
				break;
				case 'amazon':
					require_once IBK_PATH . 'classes/API/IndeedAmazonS3.class.php';
					$obj = new IndeedAmazonS3($this->destination_id);
					$files = $obj->get_files_list();
					foreach ($files as $file){
						if (preg_match("#superbackup(.*)$#i", $file)){
							$is_log = explode('.', $file);
							if (isset($is_log[1]) && $is_log[1]=='log'){
								//it's a zip file
								$file_name_data = explode('_', $is_log[0]);
								if ($file_name_data[1]==$this->snapshot_id ){
									//it's a instance of our snapshot
									$source_file = $file;
								}
							}
						}
					}
					if (!empty($source_file)){
						$file = $obj->get_file($source_file, $this->temp_dir);
						if ($file){
							$obj->delete_file($source_file);
						}
					}					
				break;
			}
		}
		
		private function send_file(){
			switch ($this->destination_type){
				case 'ftp':
					if (!class_exists('IndeedFtp')){
						require_once IBK_PATH . 'classes/API/IndeedFtp.class.php';
					}
					$obj = new IndeedFtp($this->destination_id);
					$obj->login();
					$obj->send_file( $this->temp_file );
				break;
				case 'google':
					require_once IBK_PATH . 'classes/API/IndeedGoogle.class.php';
					$goo = new IndeedGoogle($this->destination_id);
					$goo->send_file( $this->temp_file );
				break;
				case 'dropbox':
					require_once IBK_PATH . 'classes/API/IndeedDropbox.class.php';
					$obj = new IndeedDropbox($this->destination_id);
					$obj->login();
					$obj->send_file($this->temp_file, basename($this->temp_file));		
				break;
				case 'amazon':
					require_once IBK_PATH . 'classes/API/IndeedAmazonS3.class.php';
					$obj = new IndeedAmazonS3($this->destination_id);
					$obj->send_file($this->temp_file);				
				break;
			}
		}
		
		/**************** debugging ***************/
		private function write_into_debug_log($message){
			$file = IBK_PATH . 'snapshot_log_debug.log';
			file_put_contents($file, $message, FILE_APPEND | LOCK_EX);
		}
		
	}//end of class IndeedSnapshotText
}//end of if
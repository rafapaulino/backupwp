<?php 
if (!class_exists('IndeedCopyFile')){
	class IndeedCopyFile{
		private $general_metas = FALSE;
		
		public function __construct(){
			require_once IBK_PATH . 'utilities.php';			
			$this->general_metas = ibk_get_general_metas();// set general metas
			$this->set_memory_limit();
		}
		
		public function get_file_from_url($url){
			/*
			 * copy file from url to temp dir
			 * @param source url
			 * @return none
			 */
			if (!$this->general_metas['ibk_backup_dir']){
				return FALSE;
			}
			$url =  esc_url_raw($url);
			if (!empty($this->general_metas['ibk_backup_dir'])){
				if (function_exists('curl_version')) {
					$end = end((explode('/', rtrim($url, '/'))));
					$zip_file_name = $end;
					@set_time_limit(900);
					$zip = WP_CONTENT_DIR . '/uploads/'. $this->general_metas['ibk_backup_dir'] . '/' . $zip_file_name;
					//getting file
					$fp = fopen($zip, 'w');
					$ch = curl_init($url);
					curl_setopt($ch, CURLOPT_FILE, $fp);
					$data = curl_exec($ch);
					curl_close($ch);
					fclose($fp);
					return $zip;
				} else {
					//write log, curl is not available
				}
			}
		}
		
		public function get_file_from_google_drive($destination_id, $fileID){
			if (!class_exists('IndeedGoogle')){
				require_once IBK_PATH . 'classes/API/IndeedGoogle.class.php';
			}
			$goo = new IndeedGoogle($destination_id);
			$goo->login();
			$zip = $goo->downloadFile($fileID, WP_CONTENT_DIR . '/uploads/'. $this->general_metas['ibk_backup_dir'] . '/' );				
			return $zip;		
		}
		
		public function get_file_from_upload(){
			/*
			 * move the uploaded file into the temp directory
			 * @param none, use the $_FILES
			 * @return none
			 */			
			if (!$this->general_metas['ibk_backup_dir']){
				return FALSE;
			}
			@set_time_limit(900);
			$move = WP_CONTENT_DIR . '/uploads/'. $this->general_metas['ibk_backup_dir'] . '/' . $_FILES["upload_file"]['name'];
			move_uploaded_file($_FILES['upload_file']['tmp_name'], $move );
			return $move;
		}
		
		public function get_file_from_ftp($destination_id, $source_file){
			/*
			 * copy zip file from ftp and put into temp directory
			 * @param id of destination (getting metas for conn)
			 * source file
			 * @return none
			 */
			if (!$this->general_metas['ibk_backup_dir']){
				return FALSE;
			}
			if (!class_exists('IndeedFtp')){
				require_once IBK_PATH . 'classes/API/IndeedFtp.class.php';
			}
			$obj = new IndeedFtp($destination_id);
			$obj->login();
			$end = end((explode('/', rtrim($source_file, '/'))));
			$zip_file_name = $end;
			$zip = WP_CONTENT_DIR . '/uploads/'. $this->general_metas['ibk_backup_dir'] . '/' . $zip_file_name;
			return $obj->copy_file_to_local($source_file, $zip);
		}

		private function set_memory_limit(){
			if ($this->general_metas['ibk_memory_limit']){
				$this->general_metas['ibk_memory_limit'] = preg_replace('/\D/', '', $this->general_metas['ibk_memory_limit']);//remove characters from string
				$limit = $this->general_metas['ibk_memory_limit'] . 'M';//put the M in limit
				ini_set('memory_limit', $limit);
			}
		}	

		public function get_file_from_dropbox($destination_id, $source_file){
			/*
			 * source file is incomplete, don't have the path, 
			 */
			require_once IBK_PATH . 'classes/API/IndeedDropbox.class.php';
			$obj = new IndeedDropbox($destination_id);
			$obj->login();
			$files = $obj->get_files();
			foreach ($files as $file){
				if (basename($file)==$source_file){
					$source_file = $file;
				}
			}
			$zip = WP_CONTENT_DIR . '/uploads/'. $this->general_metas['ibk_backup_dir'] . '/';
			return $obj->get_file($source_file, $zip);
		}
		
		public function get_file_from_amazon($destination_id, $source_file){
			require_once IBK_PATH . 'classes/API/IndeedAmazonS3.class.php';
			$obj = new IndeedAmazonS3($destination_id);
			$files = $obj->get_files_list();
			foreach ($files as $file){
				if (basename($file)==$source_file){
					$source_file = $file;
				}
			}
			$zip = WP_CONTENT_DIR . '/uploads/'. $this->general_metas['ibk_backup_dir'] . '/';
			return $obj->get_file($source_file, $zip);
		}
		
	}//end of class
}//end of if

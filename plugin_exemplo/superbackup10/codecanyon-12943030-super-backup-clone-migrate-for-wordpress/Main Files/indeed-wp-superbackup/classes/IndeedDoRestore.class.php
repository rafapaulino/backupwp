<?php 
if (!class_exists('IndeedDoRestore')){
	class IndeedDoRestore{
		private $general_metas = FALSE;
		private $temp_dir = FALSE;
		private $archive_dir = FALSE;
		private $restore_arr;// can contain: sql, wp-config.php, plugins, uploads, themes
		
		//input
		private $zip_file = FALSE;
		private $restore_files_list = FALSE;
		private $restore_tables_list = FALSE;
		private $is_migrate = FALSE;
		private $migrate_settings = FALSE;
		
		public function __construct($target_zip_file='', $table_to_restore=FALSE, $files_to_restore=FALSE, $is_migrate=FALSE, $migrate_settings=FALSE){	
			#START PROCESS
			//$this->set_restore_log("Start Process!");		
			
			///setting the input variables
			if ($target_zip_file){
				$this->zip_file = $target_zip_file;
			} else {				
				return FALSE;//if we don't have a source zip file ... end
			}
			if ($table_to_restore){
				$this->restore_tables_list = $table_to_restore;
			}
			if ($files_to_restore){
				$this->restore_files_list = $files_to_restore;		
			}
			if ($is_migrate){
				$this->is_migrate = TRUE;
			}
			if ($migrate_settings){
				$this->migrate_settings = $migrate_settings;
			}
			
			//running the actions
			require_once IBK_PATH . 'utilities.php';			
			$this->general_metas = ibk_get_general_metas();// set general metas
			$this->set_temporary_dir();
			$this->set_memory_limit();
			$unzip = $this->unzip_file();
			if ($unzip){
				$this->restore_arr = $this->what_to_restore();
				$this->restore_the_files();
				$this->restore_the_db();
				$this->indeed_rmdir_recursive($this->temp_dir);//delete the temporary files and folder
				$this->delete_zip_file();
			}
			
			#END PROCESS
			$this->set_restore_log("Process End!");
			unlink(WP_CONTENT_DIR . '/uploads/indeed-backups/' . md5("indeed-super-backup") . '_restore.log');
		}
				
		public function set_source_zip($source_zip_file=''){
			$this->zip_file = $source_zip_file;			
		}
		
		private function set_temporary_dir(){
			/*
			 * @param none
			 * @return none
			 */			
			if (!empty($this->general_metas['ibk_backup_dir'])){				
				$dir =  WP_CONTENT_DIR . '/uploads/'. $this->general_metas['ibk_backup_dir'];
				$this->temp_dir = $dir . '/' . str_replace('.zip', '', basename($this->zip_file)) . '-restore';
			}
		}

		
		private function set_memory_limit(){
			if ($this->general_metas['ibk_memory_limit']){
				$this->general_metas['ibk_memory_limit'] = preg_replace('/\D/', '', $this->general_metas['ibk_memory_limit']);//remove characters from string
				$limit = $this->general_metas['ibk_memory_limit'] . 'M';//put the M in limit
				ini_set('memory_limit', $limit);
			}
		}

				
		private function unzip_file(){
			/*
			 * extract the zip file into temporary directory
			 */
			$this->set_restore_log("Unzip File: " . basename($this->zip_file) );
			if (class_exists('ZipArchive')){
				$zip = new ZipArchive;
				$res = $zip->open($this->zip_file);
				if ($res === TRUE) {
					$zip->extractTo($this->temp_dir);
					$zip->close();
					return TRUE;
				}
			}
			return FALSE;
		}

		private function what_to_restore(){
			$this->set_restore_log("Set Files and DataBase Tables to be restored.");
			$restore = FALSE;
			$dir = new DirectoryIterator($this->temp_dir);
			foreach ($dir as $fileinfo){
				if (!$fileinfo->isDot()){
					$restore[] = $fileinfo->getFilename();
				}
			}
			return $restore;
		}		

		
		private function restore_the_files(){
			/*
			 * main function to restore the files
			 * @param none
			 * @return none
			 */
			$excluded_folders = array(  
										WP_CONTENT_DIR . '/uploads/indeed-backups',
										WP_CONTENT_DIR . '/uploads/'. $this->general_metas['ibk_backup_dir'], 
										IBK_PATH,
									);

			if ($this->restore_files_list){
				$restore_only_arr = explode(',', $this->restore_files_list);
				$restore_arr = array_intersect($restore_only_arr, $this->restore_arr);
			} else {
				$restore_arr = $this->restore_arr;
			}
			
			if (in_array('themes', $restore_arr)){
				$this->set_restore_log('Restore the "/themes" folder.');
				$this->iterate_and_restore('themes', $excluded_folders);
			}
			if (in_array('uploads', $restore_arr)){
				$this->set_restore_log('Restore the "/uploads" folder.');
				$this->iterate_and_restore('uploads', $excluded_folders);
			}
			if (in_array('plugins', $restore_arr)){
				$this->set_restore_log('Restore the "/plugins" folder.');
				$this->iterate_and_restore('plugins', $excluded_folders);
			}			
			if (in_array('wp-config.php', $restore_arr) && !$this->is_migrate){
				$this->set_restore_log('Restore the "wp-config.php" file.');
				rename($this->temp_dir . '/wp-config.php', ABSPATH . 'wp-config.php');
			}
		}
		
		private function iterate_and_restore($target, $excluded_folders){	
			/*
			 * replace files and folders
			 * @param : 
			 * - target can be upload, themes, plugins or wp-config.php
			 * - full path to excluded folders
			 * @return none
			 */
			$old_files_arr = FALSE;
			$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->temp_dir . '/' . $target), RecursiveIteratorIterator::SELF_FIRST);
			foreach ($files as $file){
				$file = str_replace('\\', '/', $file);
				if ( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) ) continue;
				$file = realpath($file);
				if (is_file($file)){
					$old_file = str_replace($this->temp_dir . '/' . $target, WP_CONTENT_DIR . '/' . $target, $file);
					foreach ($excluded_folders as $excluded_dir){
						if (strpos($old_file, $excluded_dir)!==FALSE){
							//echo $old_file,'<br/>';
							continue 2;//skip current file from moving
						}
					}
					//echo 'File: ', $file, '<br/>Old File: ',$old_file,'<br/><br/>';
					$this->move_files($file, $old_file);
					
					$old_files_arr[] = $old_file;					
				}
			}			
			
			////extra section, delete files that are included in old wp and not in the new one
			$delete_extra_files = FALSE;
			if ($delete_extra_files && $old_files_arr){
				//iterate in wp 
				$wp_files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(WP_CONTENT_DIR . '/' . $target), RecursiveIteratorIterator::SELF_FIRST);
				foreach ($wp_files as $wp_file){
					if (!in_array($wp_file, $old_files_arr)){
						//unlink($wp_file);// remove extra file
					}
				}
			}				
		}
		
		private function move_files($source, $target){
			/*
			 * @param
			 * full path to source dir, target dir
			 */	
			$current_dir = dirname($target);
			if (!file_exists($current_dir)) {					
				wp_mkdir_p($current_dir, 0777, true);
			}				
			rename( $source, $target );
		}		
		
		private function restore_the_db(){
			/*
			 * main function to restore the sql tables
			 */
			if (in_array('sql', $this->restore_arr) ){
				$sql_tables = $this->tables_to_restore();
				
				if ($this->restore_tables_list){
					
					if ($this->is_migrate && isset($this->migrate_settings['migrate_wp_table_list'])){
						$wp_tables = array_keys(ibk_get_table_list('wp'));
						$no_wp_tables = array_diff($sql_tables, $wp_tables);
						$restore_only_arr = explode(',', $this->migrate_settings['migrate_wp_table_list']);
						$arr = array_intersect($sql_tables, $restore_only_arr);
						if ($this->migrate_settings['migrate_non_wp_tables']){
							$sql_tables_arr = array_merge($arr, $no_wp_tables);
						}
					} else {	
						$restore_only_arr = explode(',', $this->restore_tables_list);
						$sql_tables_arr = array_intersect($sql_tables, $restore_only_arr);						
					}				
				} else {
					$sql_tables_arr = $sql_tables;
				}
				
				////Exclude indeed tables
				if ($this->migrate_settings['exclude_indeed_tables']){
					$sql_tables_arr = array_diff($sql_tables_arr, array('indeed_backups', 'indeed_backup_metas', 'indeed_destinations', 'indeed_destination_metas'));
				}
				
				foreach ($sql_tables_arr as $sql_table){
					$this->restore_table($sql_table);					
				}
			}
				
		}
		
		private function tables_to_restore(){
			/*
			 * loop through sql folder and get name of each table that must be restored
			 * @param none
			 * @return array
			 */
			$sql_files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->temp_dir . '/sql/'), RecursiveIteratorIterator::SELF_FIRST);
			foreach ($sql_files as $sql_file){
				$name = basename($sql_file);
				if ($name[0]!='.'){
					$sql_tables[] = str_replace('.sql', '', $name);
				}
			}		
			return $sql_tables;	
		}
		
		private function restore_table($table_name){
			$this->set_restore_log("Restore Table: " . $table_name);
			if (file_exists($this->temp_dir . '/sql/' . $table_name . '.sql')){
				global $wpdb;
				$file = new SplFileObject($this->temp_dir . '/sql/' . $table_name . '.sql');
				$temp_table = $table_name . '_indeed_temp';
				$no_errors = 1;	
				
				while (!$file->eof() && $no_errors!==FALSE) {
					$q = $file->current();
					$file->next();
					if ($q){
						$no_errors = $wpdb->query($q);
					}			
				}//end of while feof
				
				unset($file);	

				if ($this->is_migrate && $table_name=='options' && $no_errors!==FALSE){
					$no_errors = $this->update_temp_options_table($temp_table);
				}
				
				if ($no_errors!==FALSE){
					//DROP THE ORIGINAL TABLE AND RENAME THE TEMP
					$original_table = $wpdb->prefix . $table_name;					
					$wpdb->query("DROP TABLE IF EXISTS `" . $original_table . "`;");
					$wpdb->query("ALTER TABLE `" . $temp_table . "` RENAME `" . $original_table . "`;");					
				} else {
					///DROP THE TEMP TABLE, THERE is some errors
					$wpdb->query('DROP TABLE ' . $temp_table);		
				}
		
			}//end of table file exists
			
		}//end of function restore_table
		
		private function update_temp_options_table($temp_table){
			/*
			 * return an array with values for options that must be replace on migrate
			 * we must save the following options before truncate : siteurl, home, *blogname, *blogdescription,*admin email
			 * WordPress Address (URL) - siteurl
			 * Site Address (URL) - home 
			 * Site Title - blogname 
			 * Tagline - blogdescription 
			 * Email address - admin_email
			 * @param temp table options name
			 * @return FALSE if there's an error
			 */
			global $wpdb;
			$saved_options = array('siteurl', 'home');
			if (!empty($this->migrate_settings['exclude_site_title'])){
				$saved_options[] = 'blogname';
			}
			if (!empty($this->migrate_settings['exclude_tagline'])){
				$saved_options[] = 'blogdescription';
			}
			if (!empty($this->migrate_settings['exclude_email'])){
				$saved_options[] = 'admin_email';
			}
			
			foreach ($saved_options as $value){
				$data = $wpdb->get_row("SELECT option_value FROM " . $wpdb->prefix . "options WHERE option_name='" . $value . "';");
				if (isset($data->option_value)){
					$no_errors = $wpdb->query("UPDATE " . $temp_table . " SET `option_value`='" . $data->option_value . "' WHERE `option_name`='" . $value . "';");
				}
			}
			return $no_errors;
		}
		
		private function indeed_rmdir_recursive($dir){
			/*
			 * @param none
			 * @return none
			 */
			if (file_exists($dir)){
				foreach (scandir($dir) as $file) {
					if ('.' === $file || '..' === $file){
						continue;
					}
					if (is_dir("$dir/$file")){
						$this->set_restore_log("Delete Temporary files and folder.");
						$this->indeed_rmdir_recursive("$dir/$file");
					}
					else {
						unlink("$dir/$file");
					}
				}
				rmdir($dir);
			}
		}
		
		private function delete_zip_file(){
			$data = explode("/", $this->zip_file);
			end($data);
			$data_arr = explode("_", current($data));
			$meta = ibk_return_metas_from_custom_db('backups', $data_arr[2]);
			if (!empty($meta['destination'])){
				$destination_type = ibk_get_destination_type($meta['destination']);
				if (!empty($destination_type) && $destination_type=='local'){
					//avoid delete file from local
					return;
				}			
			}
			$this->set_restore_log("Delete Zip file.");
			unlink($this->zip_file);
		}
		
		private function set_maintenance_mode($enable=FALSE){
			/*
			 * @param bool 
			 * @return none
			 */
			update_option('ibk_maintenance_mode', $enable);
		}
		
		private function set_restore_log($message){
			$file_path = WP_CONTENT_DIR . '/uploads/indeed-backups/' . md5("indeed-super-backup") . '_restore.log';
			$file = fopen($file_path, 'w');
			$str = serialize(array(time()=>$message));
			fwrite($file, $str);
		}
		
		/**************** debugging ***************/
		private function write_into_debug_log($message){
			$file = IBK_PATH . 'restore_debugging.log';
			file_put_contents($file, $message, FILE_APPEND | LOCK_EX);
		}
		
		
	}//end of class IndeedDoRestore
}
<?php 

function ibk_get_table_list($return_type='all'){
	/*
	 * @param : return_type - string : all, wp, non_wp
	 * @return all tables from db
	 */
	global $wpdb;
	$arr = array();
	$q = "SELECT table_name FROM information_schema.tables
				WHERE table_schema = '". DB_NAME ."'";
	$data = $wpdb->get_results($q);
	foreach ($data as $table){
		if (strpos($table->table_name, $wpdb->prefix)===0){
			$key = str_replace($wpdb->prefix, '', $table->table_name);
		} else {
			$key = $table->table_name;
		}		
		$arr[$key] = $table->table_name;
	}	
	
	//we don't want to backup indeed_logs
	if (isset($arr['indeed_logs'])){
		unset($arr['indeed_logs']);
	}
	
	if ($return_type!='all'){
		$tables_wp = $wpdb->tables('all');
		$native_wp = array();
		foreach ($tables_wp as $k=>$v){
			if (strpos($v, $wpdb->prefix)===0){
				$key = str_replace($wpdb->prefix, '', $v);
			} else {
				$key = $v;
			}
			$native_wp[$key] = $v;
		}
		
		if ($return_type=='wp'){
			return $native_wp;		
		} elseif ($return_type=='non_wp'){
			return array_diff($arr, $native_wp);
		}
	}
	return $arr;
}

function ibk_return_metas_from_custom_db($type = '', $id=false, $no_defaults_return=FALSE, $status=FALSE){
	/*
	 * @param type (string) = 'backups'/'destinations', id of current backup/destination
	 * @return array with metas
	 */
	$arr = FALSE;
	if ($type){
		switch ($type){
			case 'backups':
				$arr = array(
								'name' => '',
								'description' => '',
								'save_files' => 'all',
								'save_files_list' => '',
								'excluded_files' => '',
								//'save_db' => '',
								'save_db_table_list' => FALSE,
								'backup_interval_type' => 1,
								'cron-specified_date' => '',
								'cron-periodically' => '12',
								'specified_date' => '',
								'max_archives' => '1',
								'destination' => '',
								'admin_box_color' => '',
							);
				if ($id){
					if ($no_defaults_return){
						unset($arr);
						$arr = array();
					}
					//query to get meta from wp_indeed_backup_metas
					global $wpdb;
					$t1_exists = $wpdb->get_results('SHOW TABLES LIKE "'.$wpdb->prefix.'indeed_backups";');
					$t2_exists = $wpdb->get_results('SHOW TABLES LIKE "'.$wpdb->prefix.'indeed_backup_metas";');
					if ($t1_exists && $t2_exists){
						$data = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."indeed_backups WHERE id=".$id.";");
						if ($data){
							foreach ($data as $obj){
								$arr['name'] = $obj->name;
								$arr['create_date'] = $obj->create_date;
							}
						}
						$data = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."indeed_backup_metas WHERE backup_id=".$id.";");
						if ($data){
							foreach ($data as $obj){
								$arr[$obj->meta_name] = $obj->meta_value;
							}
						}
					}
				}
				break;
			case 'destinations':
				
				$arr = array(
								'name' => '',
								'type' => '',
								'admin_box_color' => '',
								'connected' => 0,
								'status'=>'',
							);
				
				if ($id){
					if ($no_defaults_return){
						unset($arr);
						$arr = array();
					}
					//query to get meta from wp_indeed_backup_metas
					global $wpdb;
					$t1_exists = $wpdb->get_results('SHOW TABLES LIKE "'.$wpdb->prefix.'indeed_destinations";');
					$t2_exists = $wpdb->get_results('SHOW TABLES LIKE "'.$wpdb->prefix.'indeed_destination_metas";');
					if ($t1_exists && $t2_exists){
						$q = "SELECT * FROM ".$wpdb->prefix."indeed_destinations WHERE 1=1";
						$q .= " AND id=".$id." ";
						if ($status){
							$q .= " AND status='".$status."' ";
						}
						$q .= ";";
						$data = $wpdb->get_results($q);
						if ($data){
							foreach ($data as $obj){
								$arr['name'] = $obj->name;
								$arr['type'] = $obj->type;
								$arr['create_date'] = $obj->create_date;
								$arr['status'] = $obj->status;
							}
						}
						$data = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."indeed_destination_metas WHERE destination_id=".$id.";");
						if ($data){
							foreach ($data as $obj){
								$arr[$obj->meta_name] = $obj->meta_value;
							}
						}
					}
				}											
			break;
		}		
	}
	return $arr;
}

function ibk_get_general_metas(){
	$arr = array(
					'ibk_backup_dir' => 'isnapshots',
					'ibk_backup_files' => 0,
					'ibk_email_sent' => 1,			
					'ibk_email' => get_option('admin_email'),
					'ibk_email_sent_1' => 0,
					'ibk_email_sent_2' => 0,
					'ibk_email_sent_3' => 0,
					'ibk_memory_limit' => '',
					'ibk_db_segmentation' => '100',								
				);
	$data = get_option('ibk_general_metas');
	if ($data!==FALSE){
		$arr = $data;
	} else {
		update_option('ibk_general_metas', $arr);
	}
	return $arr;
}

function ibk_save_general_metas($data){
	$arr = ibk_get_general_metas();
	
	//temp dir
	if (isset($data['ibk_backup_dir']) && isset($arr['ibk_backup_dir']) && ($data['ibk_backup_dir']!=$arr['ibk_backup_dir']) ){
		//remove old temp dir
		indeed_rmdir_recursive(WP_CONTENT_DIR . '/uploads/'. $arr['ibk_backup_dir']);
		//create new temp dir
		$dir = WP_CONTENT_DIR . '/uploads/'.$data['ibk_backup_dir'];
		if (!file_exists($dir)){
			mkdir($dir);
		}
	}
	
	foreach ($arr as $k=>$v){
		if (isset($data[$k])){
			$arr[$k] = $data[$k];
		}
	}
	update_option('ibk_general_metas', $arr);
}

function indeed_rmdir_recursive($dir, $keep_base_dir=FALSE){
	foreach (scandir($dir) as $file) {
		if ('.' === $file || '..' === $file){
			continue;
		}
		if (is_dir("$dir/$file")){
			indeed_rmdir_recursive("$dir/$file");
		}
		else {
			unlink("$dir/$file");
		}
	}
	if (!$keep_base_dir){
		rmdir($dir);
	}
}

function ibk_get_destination_type($id){
	/*
	 * @param id of a backup item
	 * @return type of destination for current backup
	 */
	global $wpdb;
	if ($id){
		$data = $wpdb->get_results('SELECT type FROM '.$wpdb->prefix.'indeed_destinations WHERE id='.$id.'; ');
		if (!empty($data[0]->type)){
			return $data[0]->type;
		}		
	}
	return FALSE;
}

function ibk_get_destination_name($id){
	global $wpdb;
	$data = $wpdb->get_row('SELECT name FROM '.$wpdb->prefix.'indeed_destinations WHERE id='.$id.'; ');
	if (isset($data->name)){
		return $data->name;
	}
	return FALSE;
}


function indeed_set_cron_job($id, $target_time){
	/*
	 * set our main cron job
	 * @param id of backup item, target_time in hours
	 * @return none
	 */
	if (wp_next_scheduled('indeed_main_job', array($id) )){//check for prev cron schedule
		wp_clear_scheduled_hook( 'indeed_main_job', array($id) );//delete prev cron job
	}	
	wp_schedule_single_event( $target_time , 'indeed_main_job', array( $id ) );
}

function indeed_get_free_space_size(){
	/*
	 * @param none
	 * @return disk free space in MB
	 */
	return round(disk_free_space("/")/ 1024 / 1024);
}

function indeed_get_dir_size($path){
	$bytestotal = 0;
    $path = realpath($path);
    if($path!==false){
        foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS)) as $object){
            $bytestotal += $object->getSize();
        }
    }
    return $bytestotal;
}

function indeed_from_byte_to_mb_gb($num, $extra_divide=FALSE){
	$num = $num / 1024 / 1024;
	if ($extra_divide) $num = $num / $extra_divide;
	if ($num>1024){
		$num = $num / 1024;
		return round($num, 1) . ' GB';
	}
	return round($num, 1) . ' MB';
}


function ibk_formated_time_for_dashboard($date){
	/*
	 * @param target time as timestamp
	 * @return target time retunr as : last x minutes/hours/days ago
	 */
	$return = FALSE;
	$diff = (int)time() - (int)$date;
	if ($diff<3600){
		//minutes
		$return = round($diff/60);
		$return .= ' minutes';		
	} elseif($diff>(60*60) && $diff<(60*60*24)){
		//hours
		$return = round($diff/(60*60));
		$return .= ' hours';
	} else {
		//days
		$return = round($diff/(60*60*24));
		$return .= ' days';
	}
	return $return; 
}

function indeed_count_dir_subdirs($path){
	$count = 0;
	if ($handle = opendir($path)) {
		while (false !== ($entry = readdir($handle))) {
			if ($entry=='.' || $entry=='..')continue;
			if (is_dir($path.$entry)) $count++;
		}
	}
	return $count;
}

function ibk_get_complete_percetage_for_log($data){
	/*
	 * @param data object that holds current log
	 * @return complete percetage for current process
	 */
	$complete = 0;
	end($data);
	$last_key = key($data);
	if (isset($data[$last_key]->stage)){
		switch ($data[$last_key]->stage){
			case 'start':
				$complete = 10;
				break;
					
			case 'sql':
				$complete = 30;
				break;
					
			case 'zip':
				$complete = 60;
				break;
					
			case 'sending_file':
				$complete = 90;
				break;
					
			case 'delete_zip':
				$complete = 99;
				break;
					
			case 'finish':
				$complete = 100;
				break;
		}
	}
	return $complete;
}

function ibk_return_destination_types(){
	$arr = array(
				'local' => 'Local',
				'ftp' => 'FTP',
				'dropbox' => 'DropBox',
				'google' => 'Google Drive',	
				'rackspace' => 'RackSpace',
				'amazon' => 'Amazon',
				);
	return $arr;
}

function ibk_return_active_snapshots_nr(){
	global $wpdb;
	$data = $wpdb->get_row("SELECT COUNT(*) as c FROM ".$wpdb->prefix."indeed_backups;");
	if (isset($data->c)) return $data->c;
	return 0;
}

function indeed_delete_dir_recursive($dir){
	/*
	 * delete entire folder
	 * @param full path of dir
	 * @return none
	 */
	if (file_exists($dir)){
		foreach (scandir($dir) as $file) {
			if ('.' === $file || '..' === $file){
				continue;
			}
			if (is_dir("$dir/$file")){
				indeed_delete_dir_recursive("$dir/$file");
			}
			else {
				unlink("$dir/$file");
			}
		}
		rmdir($dir);
	}
}
<ul>
<?php
try
{

	$zip = new ZipArchive();
	$ret = $zip->open('wordpress.zip', ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE);
	//var_dump($ret);

        /*** freds home directory ***/
        $rdi = new recursiveDirectoryIterator('wp-content', FilesystemIterator::SKIP_DOTS | FilesystemIterator::UNIX_PATHS);
        $it = new recursiveIteratorIterator( $rdi );
        while( $it->valid())
        {
            if (!$it->isDot() &&  !$it->isDir()) {  
            	echo '<li>
            	<ul>
            	<li><strong>Real path:</strong> '.$it->getRealPath().'</li>
            	<li><strong>Key:</strong> '.$it->key().'</li>
            	<li><strong>Name:</strong> '.$it->current()->getFilename().'</li>
            	</ul>
            	</li>';

            	if (!file_exists($it->getRealPath())) { die($it->getRealPath().' does not exist'); }
      			if (!is_readable($it->getRealPath())) { die($it->getRealPath().' not readable'); }

            	$f = $zip->addFile($it->key());
            	//var_dump($f);
            }

                /*** move to the next element ***/
                $it->next();
        }
    $close = $zip->close();
}
catch(Exception $e)
{
        /*** echo the error message ***/
        echo $e->getMessage();
}
?>
</ul>
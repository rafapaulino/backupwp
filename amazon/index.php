<?php
//https://github.com/aws/aws-sdk-php
// Require the Composer autoloader.
require 'vendor/autoload.php';

use Aws\S3\S3Client;

$bucket = 'wordpressbkp';

//https://console.aws.amazon.com/iam/home?region=us-west-2#security_credential
// Instantiate an Amazon S3 client.
$s3 = new S3Client([
    'version' => 'latest',
    'region'  => 'sa-east-1',
    //'debug'   => true,
    'credentials' => [
        'key'    => 'AKIAI7TFAFS7A5UV67JA',
        'secret' => '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'
    ]
]);

//http://docs.aws.amazon.com/AmazonS3/latest/gsg/CreatingABucket.html
// Upload a publicly accessible file. The file size and type are determined by the SDK.
try {
    $s3->putObject([
        'Bucket' => 'wordpressbkp',
        'Key'    => 'imagem.jpg',
        'Body'   => fopen('imagem.jpg', 'r'),
        'ACL'    => 'public-read',
    ]);
} catch (Aws\Exception\S3Exception $e) {
    echo "There was an error uploading the file.\n";
}

try {
    $s3->putObject([
        'Bucket' => 'wordpressbkp',
        'Key'    => 'acesso-conta.png',
        'Body'   => fopen('acesso-conta.png', 'r'),
        'ACL'    => 'public-read',
    ]);
} catch (Aws\Exception\S3Exception $e) {
    echo "There was an error uploading the file.\n";
}

try {
    $s3->putObject([
        'Bucket' => 'wordpressbkp',
        'Key'    => 'acesso-conta2.png',
        'Body'   => fopen('acesso-conta2.png', 'r'),
        'ACL'    => 'public-read',
    ]);
} catch (Aws\Exception\S3Exception $e) {
    echo "There was an error uploading the file.\n";
}

//deletar objetos
//http://docs.aws.amazon.com/AmazonS3/latest/dev/DeletingMultipleObjectsUsingPHPSDK.html
//http://docs.aws.amazon.com/AmazonS3/latest/dev/DeletingOneObjectUsingPHPSDK.html

// Delete objects from a bucket
$result = $s3->deleteObject(array(
    'Bucket' => $bucket,
    'Key'    => 'acesso-conta2.png'
));    


//lista os objetos
//http://docs.aws.amazon.com/AmazonS3/latest/dev/RetrieveObjSingleOpPHP.html

// 2. List the objects and get the keys.
//$objetos = $s3->listObjects(array('Bucket' => $bucket));
    
//echo '<pre>';
//var_dump($objetos);
//http://docs.aws.amazon.com/AmazonS3/latest/dev/ListingObjectKeysUsingPHP.html
$objects = $s3->getIterator('ListObjects', array('Bucket' => $bucket));

foreach ($objects as $object) {
    echo $object['Key'] . "<br>";
}